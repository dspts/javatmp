/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
//import points.Points.Point;

/**
 *
 * @author alex
 */
public class Measurements {

    public String filename;
    public List<Point2D> listOfPragm = new ArrayList<>();
    public List<Point2D> listOfMetr = new ArrayList<>();
    public List<Point2D> numMean = new ArrayList<>();
    public List<Point2D> weightedList = new ArrayList<>();

    Measurements(String filename) {
        this.filename = filename;
    }

    public void fileread() {
        try (BufferedReader reader =
                     new BufferedReader(new FileReader(this.filename))) {
            String line = reader.readLine();
            while (line != null) {
                String[] lone;
                // split on non arithmetic characters, 
                // first item is empty string
                lone = Arrays.copyOfRange(line.split("[^0123456789]+"), 1, 5);
                var point = new Point2D.Double(
                        Double.parseDouble(lone[0]),
                        Double.parseDouble(lone[1])
                );
                this.listOfPragm.add(point);
                Point2D point2 = new Point2D.Double(
                        Double.parseDouble(lone[2]),
                        Double.parseDouble(lone[3])
                );
                this.listOfMetr.add(point2);
                line = reader.readLine();
            }

            reader.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        //return new List[] {listOfPragm, listOfMetr};
    }

    /**
     * This method returns the arithmetic mean filter values
     * in Point2D List.
     * @param listInput contains Point2D measurements
     * @return list of arithmetic mean filter values in newList
     * **Note1** Use of ArrayList in order to store Point2D items
     * so that insertion order is maintained, duplicate elements can be stored,
     * there is no size limit etc
     * **Note2** The averaging process in the for loop could be
     * replaced partly with use of Collectors.averagingDouble()
     * as is done in mesiApoklisi()
     */
    public List<Point2D> arithMean(List<Point2D> listInput) {
        //List<Point2D> numMean = new ArrayList<>();  // arrayList to store the result
        for (var position=1; position<listInput.size(); position++) {
            numMean.add(new Point2D.Double(
                    listInput.subList(position>=10?(position-10):0, position).stream()
                            .collect(Collectors.averagingDouble(item -> item.getX())),
                    listInput.subList(position>=10?(position-10):0, position).stream()
                            .collect(Collectors.averagingDouble(item -> item.getY()))
            ));
        }
        return numMean;
    }

    /**
     *
     * @param listInput contains Point2D measurements
     * @return list of weighted mean filter values in weightedList
     * To compute the weighted average, the last 10 measurements are used 
     * if they exist.
     * **Note1** 
     * **Note2** The averaging process in the for loop could be
     * replaced partly with use of Collectors.averagingDouble()
     * as is done in mesiApoklisi()
     */
    public List<Point2D> weightedMean(List<Point2D> listInput) {
        //List<Point2D> weightedList = new ArrayList<>();
        List<Integer> weights = new ArrayList<> ();

        // weights will have the necessary weights {10,9,8,7,..}
        for (var i=1; i<=10; i++)
            weights.add(i);

        for (var i=1; i<listInput.size(); i++) {
            int arxi = (i-10)>=0?(i-10):0;
            int telos = (i<weights.size())?(10-i):0;
            List<Point2D> curr2 = listInput.subList(arxi, i);
            List<Point2D> curr3 = new ArrayList<> ();
            // Note that changing 
            List<Integer> wghts = weights.subList(telos,10);

            for (var j = 0; j < curr2.size(); j++) {
                curr3.add(new Point2D.Double(curr2.get(j).getX()*wghts.get(j),
                        curr2.get(j).getY()*wghts.get(j))
                );
            }

            Point2D pointnew = new Point2D.Double(
                    curr3.stream().map(ar -> ar.getX())
                            .mapToDouble(Double::valueOf)
                            .sum()/(wghts.stream()
                            .mapToDouble(Double::valueOf).sum()),
                    curr3.stream().map(ar -> ar.getY())
                            .mapToDouble(Double::valueOf)
                            .sum()/(wghts.stream()
                            .mapToDouble(Double::valueOf).sum())
            );
            this.weightedList.add(pointnew);
        }
        return weightedList;
    }

    /**

     *
     * @param times1 - List of Point2D values of measurements (or actual values) 
     * @param times2 - List of Point2D values of measurements (or actual values)
     * @return mean deviation
     *
     */
    public static double mesiApoklisi(List<Point2D> times1,
                                      List<Point2D> times2) {
        Iterator<Point2D> i1 = times1.iterator();
        Iterator<Point2D> i2 = times2.iterator();
        List<Double> arr = new ArrayList<> ();
        while (i1.hasNext() && i2.hasNext()) {
            var temp1 = i1.next();
            var temp2 = i2.next();
            arr.add(Math.sqrt(temp1.distance(temp2)));
            //arr.add(Math.sqrt(Math.pow(temp1.getX()-temp2.getX(), 2)
            //            + Math.pow(temp2.getY()-temp2.getY(), 2)
            //));
        }

        return arr.stream().collect(Collectors.averagingDouble(item -> item));
    }

}
