
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.geom.Point2D;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alex
 */
public class Points {

    private static String filepath;
    private final DrawingPanel panel = new DrawingPanel();
    private static final JLabel jlab = new JLabel();

    List<Point2D> listOfPragm = new ArrayList<>();
    List<Point2D> listOfMetr = new ArrayList<>();
    List<Point2D> listOfArithmetic = new ArrayList<>();
    List<Point2D> listOfWeighted = new ArrayList<>();

    java.util.List<Point> points = new ArrayList<>();
    java.util.List<Point> points2 = new ArrayList<>();
    java.util.List<Point> points3 = new ArrayList<>();
    java.util.List<Point> points4 = new ArrayList<>();


    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable(){
            public void run() {
                new Points();
            }
        });
    }

    private void filechoose() {
        JFileChooser chooser = new JFileChooser();
        chooser.showOpenDialog(null);
        java.io.File file = chooser.getSelectedFile();
        System.out.println("You chose to open this file: " + file.getName());
        System.out.println("The full path is: " + file.getPath());
        filepath = file.getPath();
        var meas = new Measurements(filepath);
        meas.fileread();
        this.listOfPragm = meas.listOfPragm;
        this.listOfMetr = meas.listOfMetr;
        this.listOfArithmetic = meas.arithMean(listOfMetr);
        this.listOfWeighted = meas.weightedMean(listOfMetr);
    }

    public Points() {
        JFrame frame = new JFrame("Plot measurements");
        frame.add(panel);
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Menu");
        menuBar.add(menu);
        JMenuItem fileopener = new JMenuItem("Open File");
        fileopener.addActionListener(e -> filechoose());
        menu.add(fileopener);
        frame.setJMenuBar(menuBar);
        frame.add(createButton(), BorderLayout.PAGE_END);
        jlab.setPreferredSize(new Dimension(500, 100));
        jlab.setBackground(Color.ORANGE);
        jlab.setText("<html>Open Menu in order to provide the file to read measurements from.<br>"
                + "Then click button to plot the data. </html>");
        frame.add(jlab, BorderLayout.NORTH);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private JButton createButton() {
        JButton button = new JButton("Plot Points");
        button.addActionListener((ActionEvent e) -> {

            listOfPragm.forEach(point -> {
                panel.addPoint(new Point((int)point.getX(),
                        (int)point.getY()), points);
            });

            listOfMetr.forEach(point -> {
                panel.addPoint(new Point((int)point.getX(),
                        (int)point.getY()), points2);
            });

            listOfArithmetic.forEach(point -> {
                panel.addPoint(new Point((int)point.getX(),
                        (int)point.getY()), points3);
            });

            listOfWeighted.forEach(point -> {
                panel.addPoint(new Point((int)point.getX(),
                        (int)point.getY()), points4);
            });
            if (filepath != null) {
                Points.jlab.setText("<html>Mean Deviation of Real and Measured Values: " +
                        String.format("%.2f", Measurements.mesiApoklisi(listOfPragm, listOfMetr)) + "<br>" +
                        "Mean Deviation of Real Values and Average Mean Filter: " +
                        String.format("%.2f", Measurements.mesiApoklisi(listOfPragm, listOfArithmetic)) + "<br>" +
                        "Mean Deviation of Real Values and Weighted Average Mean Filter: " +
                        String.format("%.2f", Measurements.mesiApoklisi(listOfPragm, listOfWeighted)) + "<br>" +
                        "</html>"
                );
            }
        });
        return button;
    }

    public class Point extends Point2D {
        double x, y;
        Color color;

        public Point(double x, double y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public void setLocation(double x, double y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public double getX() {
            return this.x;
        }

        @Override
        public double getY() {
            return this.y;
        }

        private double distance(Point p) {
            return Math.sqrt(
                    Math.pow(this.x - p.getX(), 2) +
                            Math.pow(this.y - p.getY(), 2)
            );
        }

        public void draw(Graphics g) {
            g.fillOval((int)x, (int)y, 3, 3);
        }

    }

    class DrawingPanel extends JPanel {

        DrawingPanel() {
            this.setBackground(Color.WHITE);
        }

        @Override
        protected void paintComponent(Graphics g) {
            Graphics2D g2d = (Graphics2D)g;
            super.paintComponent(g2d);
            g2d.setStroke( new BasicStroke(3) );

            g2d.setColor(Color.GREEN);
            points.forEach(point -> {
                point.draw(g2d);
            });

            g2d.setColor(Color.BLUE);
            points2.forEach(point -> {
                point.draw(g2d);
            });

            g2d.setColor(Color.RED);
            points3.forEach(point -> {
                point.draw(g2d);
            });

            g2d.setColor(Color.MAGENTA);
            points3.forEach(point -> {
                point.draw(g2d);
            });

        }

        public void addPoint(Point point, List<Point> points) {
            points.add(point);
            repaint();
        }

        @Override
        public java.awt.Dimension getPreferredSize() {
            return new java.awt.Dimension(600, 500);
        }
    }
}
