# Project Measurements
#### This is a small project implemented for one of my Masters' exercises.

It consists of a *Java Swing GUI*, through which a user can open a file containing measurements. The program reads the data (which are (x,y) points, real values and measurements), puts them in proper data structures and then plots them on a JPanel. At the same time, mean deviation of real data and measurements, mean deviation of real data and arithmetic filter data, and mean deviation of real data and weighted mean filter data are computed and shown on a JLabel. 

A sample execution can be seen here:

[![Java Points](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fmattryall.net%2Fimage%2Fjava-logo-small.png&f=1&nofb=1)](https://youtu.be/JstC-fFAwXw "Sample execution of Points.java")
